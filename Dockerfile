FROM python:3

WORKDIR /app

ADD app.py test.py /app/

CMD ["python", "-u", "./app.py"]
